#!groovy

def projectProperties = [
        [$class: 'BuildDiscarderProperty',strategy: [$class: 'LogRotator', numToKeepStr: '15']],
]
properties(projectProperties)


node {

    stage('Build') {
        checkout scm
        sh './gradlew --no-daemon clean classes'
    }

    stage('Test') {
        try {
            sh './gradlew --no-daemon check jacocoTestReport'

        } finally {
            step $class: 'JUnitResultArchiver', testResults: '**/build/test-results/**/TEST-*.xml'
        }
    }

    stage('Quality') {
        withSonarQubeEnv('sonarqube') {
            sh './gradlew --no-daemon --info sonarqube'
        }
    }
}

stage('Quality Gate') {
    timeout(time: 1, unit: 'HOURS') {
        def qg = waitForQualityGate()
        if (qg.status != 'OK') {
            error "Pipeline aborted due to quality gate failure: ${qg.status}"
        }
    }
}

node {
    stage('Artifacts') {
        sh './gradlew --no-daemon assemble'
    }
}
