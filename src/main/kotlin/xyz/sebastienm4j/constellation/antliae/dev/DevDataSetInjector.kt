package xyz.sebastienm4j.constellation.antliae.dev

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.annotation.Profile
import org.springframework.context.event.EventListener
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component
import xyz.sebastienm4j.constellation.antliae.domain.Board
import xyz.sebastienm4j.constellation.antliae.domain.Dashboard

/**
 * Injection d'un jeu de données pour le développement au démarrage du service.
 */
@Component
@Profile("dev")
class DevDataSetInjector(private val mongoTemplate: MongoTemplate)
{
    companion object {
        private val logger = LoggerFactory.getLogger(DevDataSetInjector::class.java)
    }

    @Value("\${dev.owner.id}")
    private lateinit var ownerId: String


    @EventListener(ApplicationReadyEvent::class)
    fun onApplicationReadyEvent()
    {
        logger.info("Injecting the development dataset...")

        injectDashboardDataSet()
    }


    private fun injectDashboardDataSet()
    {
        val collectionName = Dashboard::class.java.simpleName.toLowerCase()

        logger.debug("Drop previous [{}] collection if exist", collectionName)
        mongoTemplate.dropCollection(Dashboard::class.java)


        val devDashboard = Dashboard(name = "Développement", description = "Tableau de bord pour tout ce qui concerne du dév", owner = ownerId)
        devDashboard.boards += Board(name = "Suivi blogs techniques", description = "Flux RSS / Tweeter pour des articles techniques", default = true)
        devDashboard.boards += Board(name = "A faire (pro)", description = "Tous ce qui est à faire côté pro")
        devDashboard.boards += Board(name = "A faire (perso)", description = "Tous ce qui est à faire côté projets perso")
        devDashboard.boards += Board(name = "Agenda", description = "Agenda mixé (pro/perso)")

        logger.debug("Insert [{}]", devDashboard)
        mongoTemplate.insert(devDashboard)


        val persoDashboard = Dashboard(name = "Personnel", description = "Tableau de bord côté perso", owner = ownerId)
        persoDashboard.boards += Board(name = "Actualités", description = "Actualités nationnales et internationnales", default = true)
        persoDashboard.boards += Board(name = "Actu locale", description = "Actualité de la région et locale")
        persoDashboard.boards += Board(name = "A penser", description = "Notes, RDV, tâches à faire, ...")

        logger.debug("Insert [{}]", persoDashboard)
        mongoTemplate.insert(persoDashboard)


        val autreDashboard = Dashboard(name = "Autre", description = "Tableau de bord d'un autre utilisateur", owner = "db8e31fc-88e1-4896-afda-bd4aead614ac")
        autreDashboard.boards += Board(name = "Actualités", description = "Actualités", default = true)

        logger.debug("Insert [{}]", autreDashboard)
        mongoTemplate.insert(autreDashboard)
    }

}