package xyz.sebastienm4j.constellation.antliae.domain

data class Icon
(
    var icon: String? = null,
    var url: String? = null
)