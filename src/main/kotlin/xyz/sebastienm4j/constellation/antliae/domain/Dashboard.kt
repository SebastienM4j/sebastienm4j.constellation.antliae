package xyz.sebastienm4j.constellation.antliae.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document
data class Dashboard
(
    @Id
    var id: UUID = UUID.randomUUID(),

    var name: String = "",
    var description: String? = null,

    var icon: Icon? = null,

    var owner: String = "",

    var boards: Set<Board> = mutableSetOf()
)
