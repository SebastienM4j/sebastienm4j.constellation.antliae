package xyz.sebastienm4j.constellation.antliae.domain

import org.springframework.data.annotation.Id
import java.util.*

data class Board
(
    @Id
    var id: UUID = UUID.randomUUID(),

    var name: String = "",
    var description: String? = null,

    var icon: Icon? = null,

    var default: Boolean = false
)