package xyz.sebastienm4j.constellation.antliae.service

import org.springframework.stereotype.Service
import xyz.sebastienm4j.constellation.antliae.domain.Dashboard
import xyz.sebastienm4j.constellation.antliae.repository.DashboardRepository

/**
 * Service traitant les [tableaux de bord][Dashboard] de l'utilisateur actuellement authentifié.
 */
@Service
class UserDashboardService(private val authenticatedUserService: AuthenticatedUserService,
                           private val dashboardRepository: DashboardRepository)
{
    /**
     * @return Tous les tableaux de bord utilisés par l'utilisateur
     */
    fun getAllUsed(): List<Dashboard>
    {
        return dashboardRepository.findAllByOwner(authenticatedUserService.getId())
    }
}