package xyz.sebastienm4j.constellation.antliae.service

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

/**
 * Service concernant l'utilisateur actuellement authentifié.
 */
@Service
class AuthenticatedUserService
{
    /**
     * @return L'identifiant de l'utilisateur authentifié
     * @throws IllegalStateException S'il n'y a pas d'authentification
     */
    fun getId(): String
    {
        val authentication = SecurityContextHolder.getContext().authentication
        if(authentication is KeycloakAuthenticationToken) {
            return authentication.name
        }
        throw IllegalStateException("There is no authenticated user")
    }
}