package xyz.sebastienm4j.constellation.antliae

import org.springframework.boot.SpringApplication
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer

/**
 * Point d'entrée de l'application.
 */
@SpringBootApplication(
    exclude = [
        // Evite d'avoir l'authentification de Spring Actuator
        SecurityAutoConfiguration::class,
        ManagementWebSecurityAutoConfiguration::class
    ]
)
class AntliaeApp : SpringBootServletInitializer()
{
	override fun configure(builder: SpringApplicationBuilder): SpringApplicationBuilder
	{
		builder.sources(AntliaeApp::class.java)
        builder.application().setDefaultProperties(defaultProperties())
        return builder
	}
}

fun main(args: Array<String>)
{
    val application = SpringApplication(AntliaeApp::class.java)
    application.setDefaultProperties(defaultProperties())
    application.run(*args)
}


private fun defaultProperties() : Map<String, Any>
{
    val defaultProperties = HashMap<String, Any>()
    defaultProperties["spring.profiles.active"] = "dev"
    defaultProperties["server.port"] = 28800
    return defaultProperties
}
