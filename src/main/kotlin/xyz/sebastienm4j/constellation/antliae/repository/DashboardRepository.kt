package xyz.sebastienm4j.constellation.antliae.repository

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import xyz.sebastienm4j.constellation.antliae.domain.Dashboard
import java.util.*

@Repository
interface DashboardRepository : MongoRepository<Dashboard, UUID>
{
    fun findAllByOwner(owner: String): List<Dashboard>
}