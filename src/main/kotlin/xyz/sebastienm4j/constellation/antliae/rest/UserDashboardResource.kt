package xyz.sebastienm4j.constellation.antliae.rest

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import xyz.sebastienm4j.constellation.antliae.domain.Dashboard
import xyz.sebastienm4j.constellation.antliae.service.UserDashboardService

/**
 * Ressource REST correspondant aux [tableaux de bord][Dashboard] de l'utilisateur actuellement authentifié.
 */
@RestController
@RequestMapping("$API/dashboards")
class UserDashboardResource(private val userDashboardService: UserDashboardService)
{
    @GetMapping
    fun getAllUsed(): List<Dashboard>
    {
        return userDashboardService.getAllUsed()
    }
}