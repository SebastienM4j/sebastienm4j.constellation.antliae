package xyz.sebastienm4j.constellation.antliae.rest

const val API_VERSION = "v0.1"
const val API = "/api/$API_VERSION"