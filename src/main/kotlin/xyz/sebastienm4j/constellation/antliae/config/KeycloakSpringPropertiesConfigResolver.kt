package xyz.sebastienm4j.constellation.antliae.config

import org.keycloak.adapters.KeycloakConfigResolver
import org.keycloak.adapters.KeycloakDeployment
import org.keycloak.adapters.KeycloakDeploymentBuilder
import org.keycloak.adapters.spi.HttpFacade
import org.keycloak.representations.adapters.config.AdapterConfig
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Component

/**
 * Propriétés de configuration Spring Boot pour Keycloak.
 */
@Configuration
@ConfigurationProperties(prefix="keycloak")
class KeycloakProperties : AdapterConfig()


/**
 * Résolution de la configuration Keycloak pour Spring Security.
 *
 * Permet de configurer l'adaptateur Keycloak Spring Security de la même manière que pour
 * [l'adaptateur Spring Boot](https://www.keycloak.org/docs/latest/securing_apps/index.html#_spring_boot_adapter)
 * plutôt que de devoir utiliser le fichier `keycloak.json`.
 */
@Component
class KeycloakSpringPropertiesConfigResolver(private val properties: KeycloakProperties) : KeycloakConfigResolver
{
    private var deployment: KeycloakDeployment? = null

    override fun resolve(facade: HttpFacade.Request?): KeycloakDeployment
    {
        if(deployment != null) {
            return deployment as KeycloakDeployment
        }

        deployment = KeycloakDeploymentBuilder.build(properties)
        return deployment as KeycloakDeployment
    }
}