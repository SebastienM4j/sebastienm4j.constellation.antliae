package xyz.sebastienm4j.constellation.antliae.config

import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.FilterType
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.web.authentication.session.NullAuthenticatedSessionStrategy
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy

/**
 * Configuration de la sécurité avec Spring Security et Keycloak.
 *
 * On utilise [l'adapteur Spring Security](https://www.keycloak.org/docs/latest/securing_apps/index.html#_spring_security_adapter)
 * malgré que l'on soit dans une application Spring Boot. En effet, l'adaptateur Spring Boot ne fonctionne pas si le war est déployé sur un serveur
 * d'applications. [KeycloakSpringPropertiesConfigResolver] permet de pouvoir utiliser les propriétés Spring Boot pour définir la configuration.
 */
// FIXME bug dans l'adaptateur Spring Security de Keycloak 4.4.0.Final+
// En attendant un correctif, l'annotation @KeycloakConfiguration est désactivée au profit de la version ci-dessous
// Cf :
// - https://stackoverflow.com/a/53318548
// - https://issues.jboss.org/browse/KEYCLOAK-8725?focusedCommentId=13660313&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel
// - https://issues.jboss.org/browse/KEYCLOAK-8813
//@KeycloakConfiguration
@Configuration
@ComponentScan(
    basePackageClasses = [KeycloakSecurityComponents::class],
    excludeFilters = [ComponentScan.Filter(type = FilterType.REGEX, pattern = ["org.keycloak.adapters.springsecurity.management.HttpSessionManager"])]
)
@EnableWebSecurity
class SecuritySpringConfig : KeycloakWebSecurityConfigurerAdapter()
{
    override fun configure(auth: AuthenticationManagerBuilder?)
    {
        auth?.authenticationProvider(keycloakAuthenticationProvider())
    }

    override fun sessionAuthenticationStrategy(): SessionAuthenticationStrategy
    {
        return NullAuthenticatedSessionStrategy()
    }

    override fun configure(http: HttpSecurity?)
    {
        super.configure(http)

        http?.authorizeRequests()?.anyRequest()?.authenticated()
    }
}