package xyz.sebastienm4j.constellation.antliae.service

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.keycloak.KeycloakPrincipal
import org.keycloak.adapters.RefreshableKeycloakSecurityContext
import org.keycloak.adapters.springsecurity.account.SimpleKeycloakAccount
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder

@RunWith(MockitoJUnitRunner::class)
class AuthenticatedUserServiceTest
{
    private val securityContext: SecurityContext = mock()

    private val service = AuthenticatedUserService()


    @Test(expected = IllegalStateException::class)
    fun `getId throw IllegalStateException if no authenticated user`()
    {
        SecurityContextHolder.clearContext()

        service.getId()
    }

    @Test
    fun `getId returns the Keycloak user id`()
    {
        val keycloakUserId = "abcdef"

        val principal = KeycloakPrincipal<RefreshableKeycloakSecurityContext>(keycloakUserId, RefreshableKeycloakSecurityContext())
        val keycloakAccount = SimpleKeycloakAccount(principal, emptySet(), principal.keycloakSecurityContext)
        val authentication = KeycloakAuthenticationToken(keycloakAccount, false)

        whenever(securityContext.authentication).thenReturn(authentication)

        SecurityContextHolder.setContext(securityContext)

        assertThat(service.getId()).isEqualTo(keycloakUserId)
    }
}