package xyz.sebastienm4j.constellation.antliae.rest

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.keycloak.KeycloakPrincipal
import org.keycloak.adapters.RefreshableKeycloakSecurityContext
import org.keycloak.adapters.springsecurity.account.SimpleKeycloakAccount
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import xyz.sebastienm4j.constellation.antliae.domain.Board
import xyz.sebastienm4j.constellation.antliae.domain.Dashboard
import xyz.sebastienm4j.constellation.antliae.repository.DashboardRepository

@RunWith(SpringRunner::class)
@SpringBootTest
@ActiveProfiles("test")
class UserDashboardResourceIntegrationTest
{
    private lateinit var mvc: MockMvc

    private val securityContext: SecurityContext = mock()

    @Autowired
    private lateinit var userDashboardResource: UserDashboardResource

    @Autowired
    private lateinit var dashboardRepository: DashboardRepository


    @Before
    fun setUp()
    {
        mvc = MockMvcBuilders.standaloneSetup(userDashboardResource).build()


        val principal = KeycloakPrincipal<RefreshableKeycloakSecurityContext>("68c46c3f-7dd5-419f-aba9-1148700949ef", RefreshableKeycloakSecurityContext())
        val keycloakAccount = SimpleKeycloakAccount(principal, emptySet(), principal.keycloakSecurityContext)
        val authentication = KeycloakAuthenticationToken(keycloakAccount, false)

        whenever(securityContext.authentication).thenReturn(authentication)

        SecurityContextHolder.setContext(securityContext)


        val devDashboard = Dashboard(name = "Dev Dashboard", description = "Tableau de bord créé pour le développement du projet", owner = "68c46c3f-7dd5-419f-aba9-1148700949ef")
        val homeBoard = Board(name = "Accueil", description = "Planche d'accueil", default = true)
        devDashboard.boards += homeBoard
        val feedBoard = Board(name = "Suivi blogs techniques", description = "Flux RSS / Tweeter pour des articles techniques")
        devDashboard.boards += feedBoard
        dashboardRepository.save(devDashboard)

        val actuDashboard = Dashboard(name = "Actu Dashboard", description = "Tableau de bord pour suivre l'actualité", owner = "68c46c3f-7dd5-419f-aba9-1148700949ef")
        val localBoard = Board(name = "Actu locale", description = "Actualité de ma région", default = true)
        actuDashboard.boards += localBoard
        val franceBoard = Board(name = "Actu France", description = "Actualité France")
        actuDashboard.boards += franceBoard
        val mondeBoard = Board(name = "Actu Monde", description = "Actualité Mondiale")
        actuDashboard.boards += mondeBoard
        dashboardRepository.save(actuDashboard)

        val otherDashboard = Dashboard(name = "Autre Dashboard", description = "Tableau de bord de quelqu'un d'autre", owner = "007d555f-5c60-4a11-9e5e-9b182a616952")
        dashboardRepository.save(otherDashboard)
    }


    @Test
    fun `getAllUsed returns all dashboards of current user`()
    {
        mvc.perform(MockMvcRequestBuilders
            .get("$API/dashboards"))
            .andDo(print())
            .andExpect(status().isOk)
            .andExpect(jsonPath("$").isArray)
            .andExpect(jsonPath("$").isNotEmpty)
            .andExpect(jsonPath("$[0].name", Matchers.`is`("Dev Dashboard")))
            .andExpect(jsonPath("$[1].name", Matchers.`is`("Actu Dashboard")))
    }

}